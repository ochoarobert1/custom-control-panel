<?php
if ($_SERVER['REMOTE_ADDR'] == '::1') {
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp_idl/wp-load.php' );
} else {
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
}
global $wpdb;

/* --------------------------------------------------------------
    OBTENER DATA VIA POST AJAX Y ORGANIZARLA EN UN ARRAY
-------------------------------------------------------------- */







if ((isset($_POST['artistsText'])) || (isset($_POST['servicesText']))) {
    $artistsText = $_POST['artistsText'];
    $servicesText = $_POST['servicesText'];
    $multivalue = get_option( '_ccp_artists_text' );
    if ($multivalue === false) {
        add_option( '_ccp_artists_text', $artistsText, '', 'no' );
    } else {
        update_option( '_ccp_artists_text', $artistsText );
    }

    $multivalue = get_option( '_ccp_services_text' );
    if ($multivalue === false) {
        add_option( '_ccp_services_text', $servicesText, '', 'no' );
    } else {
        update_option( '_ccp_services_text', $servicesText );
    }
}

if ((isset($_POST['ccp_google_code'])) || (isset($_POST['ccp_facebook_code'])))  {
    $google_code = $_POST['ccp_google_code'];
    $facebook_code = $_POST['ccp_facebook_code'];

    $multivalue = get_option( '_ccp_google_code' );
    if ($multivalue === false) {
        add_option( '_ccp_google_code', $google_code, '', 'no' );
    } else {
        update_option( '_ccp_google_code', $google_code );
    }

    $multivalue = get_option( '_ccp_facebook_code' );
    if ($multivalue === false) {
        add_option( '_ccp_facebook_code', $facebook_code, '', 'no' );
    } else {
        update_option( '_ccp_facebook_code', $facebook_code );
    }
}

if ( isset($_POST['latitude'])) {
    $latitude = $_POST['latitude'];
    $longitude = $_POST['longitude'];

    $multivalue = get_option( '_ccp_map_latitude' );
    if ($multivalue === false) {
        add_option( '_ccp_map_latitude', $latitude, '', 'no' );
    } else {
        update_option( '_ccp_map_latitude', $latitude );
    }

    $multivalue = get_option( '_ccp_map_longitude' );
    if ($multivalue === false) {
        add_option( '_ccp_map_longitude', $longitude, '', 'no' );
    } else {
        update_option( '_ccp_map_longitude', $longitude );
    }
}
