<?php
if ($_SERVER['REMOTE_ADDR'] == '::1') {
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp_elvzlanobr/wp-load.php' );
} else {
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
}
global $wpdb;

/* --------------------------------------------------------------
    OBTENER DATA VIA POST AJAX Y ORGANIZARLA EN UN ARRAY
-------------------------------------------------------------- */

$curl = curl_init();
date_default_timezone_set('America/Santiago');
$date = date('Y-m-d\TH:i:s');
$today = date('Y-m-d');
$hoy = $today . 'T00:00:00';
$urltocurl = "http://elvenezolanonews.com/wp-json/wp/v2/posts?&filter[posts_per_page]=150&filter[order]=DESC&filter[date_query][before]=".$date. "&filter[date_query][after]=".$hoy;
curl_setopt ($curl, CURLOPT_URL, $urltocurl);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
curl_setopt($curl, CURLOPT_TIMEOUT, 0);
$result = curl_exec ($curl);
curl_close ($curl);
$response = array();
$result2 = json_decode($result);
foreach ($result2 as $item) {
    $fivesdrafts = $wpdb->get_results(
        "SELECT ID FROM $wpdb->posts WHERE post_name LIKE '$item->slug'"
    );
    if (!$fivesdrafts) {

        $my_post = array(
            'post_title'    => wp_strip_all_tags( $item->title->rendered ),
            'post_content'  => $item->content->rendered,
            'post_status'   => 'publish',
            'post_author'   => $item->author,
            'post_excerpt'  => $item->excerpt->rendered,
            'post_date'     => $item->date,
            'post_name'     => $item->slug
        );
        // Insert the post into the database
        $postid = wp_insert_post( $my_post );
        $post_categories = $item->categories;

        wp_set_post_categories( $postid, $post_categories );

        $post_tags = $item->tags;

        wp_set_post_tags( $postid, $post_tags );

        $urltocurl = 'http://elvenezolanonews.com/wp-json/wp/v2/media/'. $item->featured_media;

        $curl2 = curl_init();
        curl_setopt ($curl2, CURLOPT_URL, $urltocurl);
        curl_setopt($curl2, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl2, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($curl2, CURLOPT_TIMEOUT, 0);
        $result = curl_exec ($curl2);
        curl_close ($curl2);
        $response = array();
        $result3 = json_decode($result);
        $image = $result3->guid->rendered;

        $resultado = media_sideload_image($image, $postid);
        $resultado = str_replace("<img src='","", $resultado);
        $resultado = str_replace("' alt='' />","", $resultado);

        $query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$resultado'";
        $attachment_id = $wpdb->get_var($query);

        set_post_thumbnail( $postid, $attachment_id );

    } else {
        echo 'ya existe <br />';
    }
}
