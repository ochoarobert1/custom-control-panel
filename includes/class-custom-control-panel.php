<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.robertochoa.com.ve/
 * @since      1.0.0
 *
 * @package    Custom_Control_Panel
 * @subpackage Custom_Control_Panel/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Custom_Control_Panel
 * @subpackage Custom_Control_Panel/includes
 * @author     Robert Ochoa <ochoa.robert1@gmail.com>
 */
class Custom_Control_Panel {

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Custom_Control_Panel_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $plugin_name    The string used to uniquely identify this plugin.
     */
    protected $plugin_name;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $version    The current version of the plugin.
     */
    protected $version;

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct() {

        $this->plugin_name = 'custom-control-panel';
        $this->version = '1.0.0';

        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->define_public_hooks();
        $this->define_custom_hooks();

    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - Custom_Control_Panel_Loader. Orchestrates the hooks of the plugin.
     * - Custom_Control_Panel_i18n. Defines internationalization functionality.
     * - Custom_Control_Panel_Admin. Defines all hooks for the admin area.
     * - Custom_Control_Panel_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function load_dependencies() {

        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-custom-control-panel-loader.php';

        /**
         * The class responsible for defining internationalization functionality
         * of the plugin.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-custom-control-panel-i18n.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-custom-control-panel-admin.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-custom-control-panel-public.php';

        $this->loader = new Custom_Control_Panel_Loader();

    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the Custom_Control_Panel_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function set_locale() {

        $plugin_i18n = new Custom_Control_Panel_i18n();

        $this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_admin_hooks() {

        $plugin_admin = new Custom_Control_Panel_Admin( $this->get_plugin_name(), $this->get_version() );

        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );


        // Add menu item
        $this->loader->add_action( 'admin_menu', $plugin_admin, 'add_plugin_admin_menu' );

        // Add Settings link to the plugin
        $plugin_basename = plugin_basename( plugin_dir_path( __DIR__ ) . $this->plugin_name . '.php' );
        $this->loader->add_filter( 'plugin_action_links_' . $plugin_basename, $plugin_admin, 'add_action_links' );

    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_public_hooks() {

        $plugin_public = new Custom_Control_Panel_Public( $this->get_plugin_name(), $this->get_version() );

        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
        if (isset($_GET['page'])) {
            $current_screen = $_GET['page'];
            $screens = array('custom-control-panel', 'custom-areas', 'datos-contacto', 'codigo-tracking', 'ubicacion-mapas', 'manuales-uso', 'subir-entrada', 'editar-pagina', 'editar-menu', 'crear-usuarios', 'config-inicio', 'config-zones', 'config-blocks', 'carga-noticias', 'fetch-news' );
            if (in_array($current_screen, $screens)) {
                add_filter( "admin_body_class", array( $this, 'ctrl_homepage_add_admin_body_class' ) );
            }
        }
        add_action( 'admin_survey', array($this, 'display_survey_chooser'));

    }



    public function display_survey_chooser() {
        /* echo '<a href="http://deunomedia.com/encuesta-del-plugin/" target="_blank"><button class="ccp-header-button">Déjanos tus Comentarios</button></a>'; */

        echo '<a href="http://robertochoa.com.ve/formulario-de-plug-in-de-control/" target="_blank"><button class="ccp-header-button">Déjanos tus Comentarios</button></a>';

        echo '<script type="text/javascript"> var ruta_blog  = "' . plugins_url() . '/' . $this->plugin_name . '"; </script>';

    }


    public function ctrl_homepage_add_admin_body_class( $classes ) {
        return "$classes custom-control-panel";
        // Or:
        // return "$classes my_class_1 my_class_2 my_class_3";
    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run() {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     1.0.0
     * @return    string    The name of the plugin.
     */
    public function get_plugin_name() {
        return $this->plugin_name;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.0.0
     * @return    Custom_Control_Panel_Loader    Orchestrates the hooks of the plugin.
     */
    public function get_loader() {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.0.0
     * @return    string    The version number of the plugin.
     */
    public function get_version() {
        return $this->version;
    }

    /**
     * ALL CUSTOM HOOKS AND FUNCTION INTO ADMIN PANEL.
     *
     * @since    1.0.0
     */
    public function define_custom_hooks() {

        function broward_news_fetcher() {

            global $wpdb;
            $curl = curl_init();
            date_default_timezone_set('America/Santiago');
            $date = date('Y-m-d\TH:i:s', time() - 3600);
            $today = date('Y-m-d');
            $hoy = $today . 'T00:00:00';
            $urltocurl = "http://elvenezolanonews.com/wp-json/wp/v2/posts?&filter[posts_per_page]=50&filter[order]=DESC&filter[date_query][before]=".$date. "&filter[date_query][after]=".$hoy;
            curl_setopt ($curl, CURLOPT_URL, $urltocurl);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
            curl_setopt($curl, CURLOPT_TIMEOUT, 0);
            $result = curl_exec ($curl);
            curl_close ($curl);
            $response = array();
            $result2 = json_decode($result);
            foreach ($result2 as $item) {
                $fivesdrafts = $wpdb->get_results(
                    "SELECT ID FROM $wpdb->posts WHERE post_name LIKE '$item->slug'"
                );
                if (!$fivesdrafts) {

                    $my_post = array(
                        'post_title'    => wp_strip_all_tags( $item->title->rendered ),
                        'post_content'  => $item->content->rendered,
                        'post_status'   => 'publish',
                        'post_author'   => $item->author,
                        'post_excerpt'  => $item->excerpt->rendered,
                        'post_date'     => $item->date,
                        'post_name'     => $item->slug
                    );
                    // Insert the post into the database
                    $postid = wp_insert_post( $my_post );
                    $post_categories = $item->categories;

                    wp_set_post_categories( $postid, $post_categories );

                    $post_tags = $item->tags;

                    wp_set_post_tags( $postid, $post_tags );

                    $urltocurl = 'http://elvenezolanonews.com/wp-json/wp/v2/media/'. $item->featured_media;

                    $curl2 = curl_init();
                    curl_setopt ($curl2, CURLOPT_URL, $urltocurl);
                    curl_setopt($curl2, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl2, CURLOPT_CONNECTTIMEOUT, 0);
                    curl_setopt($curl2, CURLOPT_TIMEOUT, 0);
                    $result = curl_exec ($curl2);
                    curl_close ($curl2);
                    $response = array();
                    $result3 = json_decode($result);
                    $image = $result3->guid->rendered;

                    $resultado = media_sideload_image($image, $postid);
                    $resultado = str_replace("<img src='","", $resultado);
                    $resultado = str_replace("' alt='' />","", $resultado);

                    $query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$resultado'";
                    $attachment_id = $wpdb->get_var($query);

                    set_post_thumbnail( $postid, $attachment_id );

                } else {
                    echo 'ya existe <br />';
                }
            }
        }

        function elvlznalo_scheduler() {
            $schedule = wp_get_schedule( 'my_hourly_event' );
            if ($schedule == FALSE){
                wp_schedule_event(time(), 'hourly', 'my_hourly_event');
                return 'activada';
            } else {
                return 'activa';
            }
        }

        add_action('my_hourly_event', 'do_this_hourly');
        function do_this_hourly() {
            broward_news_fetcher();
        }
    }

}
