<?php
if ($_SERVER['REMOTE_ADDR'] == '::1') {
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp_elvzlanobr/wp-load.php' );
} else {
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp-load.php' );
}
global $wpdb;

/* --------------------------------------------------------------
    OBTENER DATA VIA POST AJAX Y ORGANIZARLA EN UN ARRAY
-------------------------------------------------------------- */

$datos = $_POST['datos'];
$posiciones = $_POST['posiciones'];
$datos = str_replace("destacado[]=","",$datos);
$datos = explode("&", $datos);
$posiciones = explode(",", $posiciones);
print_r($datos);
print_r($posiciones);
foreach($posiciones as $bloques){
    $clave = array_search($bloques, $datos, true);
    $clave = $clave + 1;
    echo 'Bloque: ' . $bloques . ', Posicion: ' . $clave;
    echo '<br />';
    $wpdb->update(
        $wpdb->prefix . 'cc_blocks',
        array(
            'posicion' => $clave,
        ),
        array( 'Id' => $bloques ),
        array(
            '%d'
        ),
        array( '%d' )
    );
}

