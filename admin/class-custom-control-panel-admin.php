<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://www.robertochoa.com.ve/
 * @since      1.0.0
 *
 * @package    Custom_Control_Panel
 * @subpackage Custom_Control_Panel/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Custom_Control_Panel
 * @subpackage Custom_Control_Panel/admin
 * @author     Robert Ochoa <ochoa.robert1@gmail.com>
 */
class Custom_Control_Panel_Admin {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $plugin_name    The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $plugin_name       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $plugin_name, $version ) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

    }

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Custom_Control_Panel_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Custom_Control_Panel_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Roboto:100,100italic,300,300italic,400,400italic,500,500italic,700,700italic,900,900italic', array(), $this->version, 'all' );
        wp_enqueue_style( 'jquery-ui-css', plugin_dir_url( __FILE__ ) . 'css/jquery-ui.min.css', array(), '1.12.0', 'all' );
        wp_enqueue_style( 'jquery-ui-structure-css', plugin_dir_url( __FILE__ ) . 'css/jquery-ui.structure.min.css', array(), '1.12.0', 'all' );
        wp_enqueue_style( 'jquery-ui-theme-css', plugin_dir_url( __FILE__ ) . 'css/jquery-ui.theme.min.css', array(), '1.12.0', 'all' );
        wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/custom-control-panel-admin.css', array(), $this->version, 'all' );
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in Custom_Control_Panel_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Custom_Control_Panel_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_script( 'lettering', plugin_dir_url( __FILE__ ) . 'js/jquery.lettering.js', array( 'jquery' ), '0.7.0', false );

        wp_enqueue_script( 'sticky', plugin_dir_url( __FILE__ ) . 'js/jquery.sticky.js', array( 'jquery' ), '1.0.3', false );

        wp_enqueue_script( 'jquery-ui', plugin_dir_url( __FILE__ ) . 'js/jquery-ui.min.js', array( 'jquery' ), '1.12.0', false );


        wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/custom-control-panel-admin.js', array( 'jquery' ), $this->version, false );

    }

    /**
 * Register the administration menu for this plugin into the WordPress Dashboard menu.
 *
 * @since    1.0.0
 */

    public function add_plugin_admin_menu() {

        /*
     * Add a settings page for this plugin to the Settings menu.
     *
     * NOTE:  Alternative menu locations are available via WordPress administration menu functions.
     *
     *        Administration Menus: http://codex.wordpress.org/Administration_Menus
     *
     */
        add_menu_page( 'Panel de Control', 'Panel de Control', 'edit_posts', $this->plugin_name, array($this, 'display_plugin_setup_page'), 'dashicons-hammer', 3);
        /* MANUALES */
        add_submenu_page( $this->plugin_name, 'Ajustar Inicio', 'Ajustar Inicio', 'edit_posts', 'config-inicio', array($this, 'display_plugin_config_home_page') );
        add_submenu_page( '', 'Configurar Inicio', 'Configurar Inicio', 'edit_posts', 'config-zones', array($this, 'display_plugin_config_zones') );
        add_submenu_page( '', 'Configurar Inicio', 'Configurar Inicio', 'edit_posts', 'config-blocks', array($this, 'display_plugin_config_block') );
        add_submenu_page( $this->plugin_name, 'Auto Carga de Noticias', 'Auto Carga de Noticias', 'edit_posts', 'carga-noticias', array($this, 'display_plugin_auto_upload_news') );
        add_submenu_page( '', 'Cargar Noticias', 'Cargar Noticias', 'edit_posts', 'fetch-news', array($this, 'display_plugin_news_fetcher') );
        /* PERSONALIZAR */
        add_submenu_page( $this->plugin_name, 'Personalizar áreas', 'Personalizar áreas', 'edit_posts', 'custom-areas', array($this, 'display_plugin_custom_areas_page') );
        add_submenu_page( '', 'Datos de Contacto', 'Datos de Contacto', 'edit_posts', 'datos-contacto', array($this, 'display_plugin_datos_contacto') );
        add_submenu_page( '', 'Códigos de Seguimiento', 'Códigos de Seguimiento', 'edit_posts', 'codigo-tracking', array($this, 'display_plugin_codigos_tracking') );
        add_submenu_page( '', 'Geoposición en Google Maps', 'Geoposición en Google Maps', 'edit_posts', 'ubicacion-mapas', array($this, 'display_plugin_ubicacion_mapas') );
        /* MANUALES */
        add_submenu_page( $this->plugin_name, 'Manuales de Uso', 'Manuales de Uso', 'edit_posts', 'manuales-uso', array($this, 'display_plugin_manuales_uso') );
        add_submenu_page( '', 'Subir Entrada/Noticia a WordPress', 'Subir Entrada/Noticia a WordPress', 'edit_posts', 'subir-entrada', array($this, 'display_plugin_subir_entrada') );
        add_submenu_page( '', 'Editar Página del Sitio', 'Editar Página del Sitio', 'edit_posts', 'editar-pagina', array($this, 'display_plugin_editar_pagina') );
        add_submenu_page( '', 'Editar Menu', 'Editar Menu', 'edit_posts', 'editar-menu', array($this, 'display_plugin_editar_menu') );
        add_submenu_page( '', 'Crear Usuarios', 'Crear Usuarios', 'edit_posts', 'crear-usuarios', array($this, 'display_plugin_crear_usuarios') );

    }
    /**
 * Add settings action link to the plugins page.
 *
 * @since    1.0.0
 */

    public function add_action_links( $links ) {
        /*
    *  Documentation : https://codex.wordpress.org/Plugin_API/Filter_Reference/plugin_action_links_(plugin_file_name)
    */
        $settings_link = array(
            '<a href="' . admin_url( 'admin.php?page=' . $this->plugin_name ) . '">' . __('Panel de Control', $this->plugin_name) . '</a>',
        );
        return array_merge(  $settings_link, $links );

    }

 public function broward_news_fetcher() {
        return 'hola panas';
    }

    /**
 * Render the settings page for this plugin.
 *
 * @since    1.0.0
 */

    public function display_plugin_setup_page() {
        include_once( 'partials/custom-control-panel-admin-display.php' );
    }

    public function display_plugin_config_home_page() {
        include_once( 'partials/custom-control-panel-config-home-display.php' );
    }

    public function display_plugin_config_zones() {
        include_once( 'partials/home-control/custom-control-panel-config-zones.php' );
    }

    public function display_plugin_config_block() {
        include_once( 'partials/home-control/custom-control-panel-config-block.php' );
    }

    public function display_plugin_auto_upload_news() {
        include_once( 'partials/custom-control-panel-auto-upload-news.php' );
    }

    public function display_plugin_custom_areas_page() {
        include_once( 'partials/custom-control-panel-custom-areas-display.php' );
    }

    public function display_plugin_datos_contacto() {
        include_once( 'partials/custom-control-panel-datos-contacto.php' );
    }

    public function display_plugin_codigos_tracking() {
        include_once( 'partials/custom-control-panel-codigo-tracking.php' );
    }

    public function display_plugin_ubicacion_mapas() {
        include_once( 'partials/custom-control-panel-ubicacion-mapas.php' );
    }

    public function display_plugin_manuales_uso() {
        include_once( 'partials/custom-control-panel-manuales-uso.php' );
    }

    public function display_plugin_subir_entrada() {
        include_once( 'partials/custom-control-panel-subir-entrada.php' );
    }

    public function display_plugin_editar_pagina() {
        include_once( 'partials/custom-control-panel-editar-pagina.php' );
    }

    public function display_plugin_editar_menu() {
        include_once( 'partials/custom-control-panel-editar-menu.php' );
    }

    public function display_plugin_crear_usuarios() {
        include_once( 'partials/custom-control-panel-crear-usuarios.php' );
    }

    public function display_plugin_news_fetcher() {
        include_once( 'partials/custom-control-panel-news-fetcher-screen.php' );
    }


}
