<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.robertochoa.com.ve/
 * @since      1.0.0
 *
 * @package    Custom_Control_Panel
 * @subpackage Custom_Control_Panel/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<?php  ?>
<?php $themes = wp_get_theme(); ?>
<div class="ccp-main-container">
    <div class="ccp-header-container">
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-white.png" alt="Logo" class="ccp-img-brand">
        <div class="ccp-header-info">
            <h1 class="sectiontitle">Configurar Inicio del Sitio</h1>
            <p>Área para controlar los elementos del home - Elija la opcion que necesite ajustar.</p>
        </div>
        <div class="ccp-header-extra">
            <a href="<?php echo esc_url(admin_url('/themes.php?theme=' . get_template())); ?>" title="Ver información del Theme"><p><?php echo $themes->name; ?></p></a>
            <?php echo do_action( 'admin_survey' ); ?>
        </div>
    </div>
    <div class="ccp-content-container">
        <div class="ccp-section-title">
            <h2>Portada - Inicio del Sitio</h2>
        </div>
         <?php
$image = '<img src="http://www.somesite.com/wp-content/uploads/2012/11/image8.jpg" alt="" />';
$image = preg_replace('/.*(?<=src=["])([^"]*)(?=["]).*/', '$1', $image);
                                                   echo $image;
?>
        <div class="ccp-function-container">
            <a href="<?php echo esc_url(admin_url('/admin.php?page=config-zones')); ?>">
                <div class="ccp-function-item ccp-function-item-1">
                    <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon1.png" alt="">
                    <h3>Ajustar Inicio</h3>

                </div>
            </a>
            <a href="<?php echo esc_url(admin_url('/admin.php?page=config-blocks')); ?>">
                <div class="ccp-function-item ccp-function-item-2">
                    <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon2.png" alt="">
                    <h3>Ajustar Bloques</h3>
                </div>
            </a>
            <a href="<?php echo esc_url(admin_url('/admin.php?page=codigo-tracking')); ?>">
                <div class="ccp-function-item ccp-function-item-3">
                    <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon3.png" alt="">
                    <h3>Ajustar Noticias de Bloques</h3>
                </div>
            </a>
            <a href="<?php echo esc_url(admin_url('/admin.php?page=ubicacion-mapas')); ?>">
                <div class="ccp-function-item ccp-function-item-4">
                    <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon6.png" alt="">
                    <h3>Publicidad en Inicio</h3>
                </div>
            </a>
        </div>
        <div class="ccp-clearfix"></div>
    </div>
</div>
