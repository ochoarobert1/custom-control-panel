<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.robertochoa.com.ve/
 * @since      1.0.0
 *
 * @package    Custom_Control_Panel
 * @subpackage Custom_Control_Panel/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<?php global $wpdb; ?>
<?php $themes = wp_get_theme(); ?>
<div class="ccp-main-container">
    <div class="ccp-header-container">
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-white.png" alt="Logo" class="ccp-img-brand">
        <div class="ccp-header-info">
            <h1 class="sectiontitle">Auto-carga de Noticias</h1>
            <p>Áquí controlamos la carga automática de noticias.</p>
        </div>
        <div class="ccp-header-extra">
            <a href="<?php echo esc_url(admin_url('/themes.php?theme=' . get_template())); ?>" title="Ver información del Theme"><p><?php echo $themes->name; ?></p></a>
            <?php echo do_action( 'admin_survey' ); ?>
        </div>
    </div>
    <div class="ccp-content-container">
        <div class="ccp-autoupload-container">
            <div class="ccp-autoupload-button-container">
                <a href="<?php echo esc_url(admin_url('/admin.php?page=fetch-news')); ?>">
                    <button>Haga click para cargar noticias</button>
                </a>
            </div>
            <div class="ccp-autoupload-message-container">
                <p><strong>Última Carga:</strong> 20 min.</p>
                <p><strong>Carga Programada:</strong> <?php echo elvlznalo_scheduler(); ?></p>
            </div>
            <div class="ccp-clearfix"></div>
            <div class="ccp-autoupload-content">
                <div class="ccp-alert-box"></div>
                <div class="ccp-autoupload-item">
                    <?php $categoria = get_category_by_slug('venezuela');  ?>
                    <?php $args = array( 'posts_per_page' => 5, 'offset'=> 1, 'category' => $categoria->term_id ); ?>
                    <?php $myposts = get_posts( $args ); ?>
                    <h3>VENEZUELA</h3>
                    <p><?php echo count($myposts); ?> Noticias</p>
                    <button>click para cargar esta categoría</button>
                </div>
                <div class="ccp-autoupload-item">
                    <?php $categoria = get_category_by_slug('eeuu');  ?>
                    <?php $args = array( 'posts_per_page' => 5, 'offset'=> 1, 'category' => $categoria->term_id ); ?>
                    <?php $myposts = get_posts( $args ); ?>
                    <h3>EEUU</h3>
                    <p><?php echo count($myposts); ?> Noticias</p>
                    <button>click para cargar esta categoría</button>
                </div>
                <div class="ccp-autoupload-item">
                    <?php $categoria = get_category_by_slug('miami');  ?>
                    <?php $args = array( 'posts_per_page' => 5, 'offset'=> 1, 'category' => $categoria->term_id ); ?>
                    <?php $myposts = get_posts( $args ); ?>
                    <h3>MIAMI</h3>
                    <p><?php echo count($myposts); ?> Noticias</p>
                    <button>click para cargar esta categoría</button>
                </div>
                <div class="ccp-autoupload-item">
                    <?php $categoria = get_category_by_slug('deportes');  ?>
                    <?php $args = array( 'posts_per_page' => 5, 'offset'=> 1, 'category' => $categoria->term_id ); ?>
                    <?php $myposts = get_posts( $args ); ?>
                    <h3>DEPORTES</h3>
                    <p><?php echo count($myposts); ?> Noticias</p>
                    <button>click para cargar esta categoría</button>
                </div>
                <div class="ccp-autoupload-item">
                    <?php $categoria = get_category_by_slug('entretenimiento');  ?>
                    <?php $args = array( 'posts_per_page' => 5, 'offset'=> 1, 'category' => $categoria->term_id ); ?>
                    <?php $myposts = get_posts( $args ); ?>
                    <h3>ENTRETENIMIENTO</h3>
                    <p><?php echo count($myposts); ?> Noticias</p>
                    <button>click para cargar esta categoría</button>
                </div>
                <div class="ccp-autoupload-item">
                    <?php $categoria = get_category_by_slug('internacionales');  ?>
                    <?php $args = array( 'posts_per_page' => 5, 'offset'=> 1, 'category' => $categoria->term_id ); ?>
                    <?php $myposts = get_posts( $args ); ?>
                    <h3>INTERNACIONALES</h3>
                    <p><?php echo count($myposts); ?> Noticias</p>
                    <button>click para cargar esta categoría</button>
                </div>
                <div class="ccp-autoupload-item">
                    <?php $categoria = get_category_by_slug('ediciones-impresas');  ?>
                    <?php $args = array( 'posts_per_page' => 5, 'offset'=> 1, 'category' => $categoria->term_id ); ?>
                    <?php $myposts = get_posts( $args ); ?>
                    <h3>EDICIONES IMPRESAS</h3>
                    <p><?php echo count($myposts); ?> Noticias</p>
                    <button>click para cargar esta categoría</button>
                </div>
                <div class="ccp-clearfix"></div>
            </div>
        </div>
    </div>
</div>
