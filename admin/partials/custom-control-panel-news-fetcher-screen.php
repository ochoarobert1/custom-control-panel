<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.robertochoa.com.ve/
 * @since      1.0.0
 *
 * @package    Custom_Control_Panel
 * @subpackage Custom_Control_Panel/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<?php global $wpdb; ?>
<?php $themes = wp_get_theme(); ?>
<div class="ccp-main-container">
    <div class="ccp-header-container">
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-white.png" alt="Logo" class="ccp-img-brand">
        <div class="ccp-header-info">
            <h1 class="sectiontitle">Auto-carga de Noticias</h1>
            <p>Áquí controlamos la carga automática de noticias.</p>
        </div>
        <div class="ccp-header-extra">
            <a href="<?php echo esc_url(admin_url('/themes.php?theme=' . get_template())); ?>" title="Ver información del Theme"><p><?php echo $themes->name; ?></p></a>
            <?php echo do_action( 'admin_survey' ); ?>
        </div>
    </div>
    <div class="ccp-content-container">
        <div class="ccp-autoupload-container">
            <div class="ccp-autoupload-button-container">

            </div>
            <div class="ccp-autoupload-message-container">
                <p><strong>Última Carga:</strong> 20 min.</p>
                <p><strong>Carga Programada:</strong> 10 min.</p>
            </div>
            <div class="ccp-clearfix"></div>
            <div class="ccp-autoupload-content">
                <?php $curl = curl_init();
                date_default_timezone_set('America/Santiago');
                $date = date('Y-m-d\TH:i:s');
                $today = date('Y-m-d');
                $hoy = $today . 'T00:00:00';
                $urltocurl = "http://elvenezolanonews.com/wp-json/wp/v2/posts?&filter[posts_per_page]=150&filter[order]=DESC&filter[date_query][before]=".$date. "&filter[date_query][after]=".$hoy;
                curl_setopt ($curl, CURLOPT_URL, $urltocurl);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 0);
                curl_setopt($curl, CURLOPT_TIMEOUT, 0);
                $result = curl_exec ($curl);
                curl_close ($curl);
                $response = array();
                $result2 = json_decode($result);
                foreach ($result2 as $item) {
                    $fivesdrafts = $wpdb->get_results(
                        "SELECT ID FROM $wpdb->posts WHERE post_name LIKE '$item->slug'"
                    );
                    if (!$fivesdrafts) {

                        $my_post = array(
                            'post_title'    => wp_strip_all_tags( $item->title->rendered ),
                            'post_content'  => $item->content->rendered,
                            'post_status'   => 'publish',
                            'post_author'   => $item->author,
                            'post_excerpt'  => $item->excerpt->rendered,
                            'post_date'     => $item->date,
                            'post_name'     => $item->slug
                        );
                        // Insert the post into the database
                        $postid = wp_insert_post( $my_post );
                        $post_categories = $item->categories;

                        wp_set_post_categories( $postid, $post_categories );

                        $post_tags = $item->tags;

                        wp_set_post_tags( $postid, $post_tags );

                        $urltocurl = 'http://elvenezolanonews.com/wp-json/wp/v2/media/'. $item->featured_media;

                        $curl2 = curl_init();
                        curl_setopt ($curl2, CURLOPT_URL, $urltocurl);
                        curl_setopt($curl2, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($curl2, CURLOPT_CONNECTTIMEOUT, 0);
                        curl_setopt($curl2, CURLOPT_TIMEOUT, 0);
                        $result = curl_exec ($curl2);
                        curl_close ($curl2);
                        $response = array();
                        $result3 = json_decode($result);
                        $image = $result3->guid->rendered;

                        $resultado = media_sideload_image($image, $postid);
                        $resultado = str_replace("<img src='","", $resultado);
                        $resultado = str_replace("' alt='' />","", $resultado);

                        $query = "SELECT ID FROM {$wpdb->posts} WHERE guid='$resultado'";
                        $attachment_id = $wpdb->get_var($query);

                        set_post_thumbnail( $postid, $attachment_id );

                    } else {
                        echo 'ya existe <br />';
                    }
                }?>
            </div>
        </div>
    </div>
</div>
