<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.robertochoa.com.ve/
 * @since      1.0.0
 *
 * @package    Custom_Control_Panel
 * @subpackage Custom_Control_Panel/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php $themes = wp_get_theme (); ?>

<div class="ccp-main-container">
    <div class="ccp-header-container">
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="IDL Logo" class="ccp-img-brand">
        <div class="ccp-header-info">
            <h1>Manuales de Uso - <?php echo $themes->name; ?></h1>
            <p>Diversos manuales para el control del sitio web.</p>
        </div>
    </div>
    <div class="ccp-content-container">
        <div class="ccp-manual-container">
            <h3>Para hacer carga de distintos miembros del equipo, debemos hacer lo siguiente:</h3>
            <p><strong>1.-</strong> Abrir el Administrador de WordPress, e ingresar en la sección de Equipo.</p>
            <p>Dentro del mismo tendremos el acceso a editar cualquiera de los miembros existentes del equipo o crear alguno nuevo.</p>
            <p>Dependiendo de lo que querramos hacer, hacemos click en "Agregar Nuevo" o en el nombre del miembro, para editarlo.</p>
            <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/assets/cargar-equipo/1.jpg" alt="" />
            <p><strong>2.-</strong>Ya dentro del editor de equipo, ingresamos como titulo de nota al miembro del equipo que querramos ingresar.</p>
            <p>En el contenido de la nota colocamos el pensamiento que querramos denotar en el sitio.</p>
            <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/assets/cargar-equipo/2.jpg" alt="" />
            <p>Del lado izquierdo vamos a ver los datos extras asi como la foto del personaje a denotar.</p>
            <p>En sus datos extras conseguiremos su Rol, en el cual podremos usar codigo HTML para acomodarlo mejor en el sitio final</p>
            <p>Adicional a esto, debajo obtendremos un espacio para colocar sus redes, si una red esta vacia, este icono <strong>NO</strong> se mostrará</p>
            <p>La foto tiene una particularidad, debe ser lo mas cuadrada posible, para que haga el efecto circular en el sitio, tratemos de usar unas imágenes que sean proporcionales 1:1</p>
            <p>Una vez finalizada su edición, haremos click en el boton azul de Actualizar o Publicar, dependiendo el caso que hayamos escogido anteriormente</p>

        </div>
    </div>
</div>
