<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.robertochoa.com.ve/
 * @since      1.0.0
 *
 * @package    Custom_Control_Panel
 * @subpackage Custom_Control_Panel/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php $themes = wp_get_theme (); ?>

<div class="ccp-main-container">
    <div class="ccp-header-container">
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-white.png" alt="Logo" class="ccp-img-brand">
        <div class="ccp-header-info">
            <h1 class="sectiontitle">Subir Entrada a WordPress</h1>
            <p>Manual de como cargar entradas / noticias a nuestro sitio.</p>
        </div>
        <div class="ccp-header-extra">
            <a href="<?php echo esc_url(admin_url('/themes.php?theme=' . get_template())); ?>" title="Ver información del Theme"><p><?php echo $themes->name; ?></p></a>
            <?php echo do_action( 'admin_survey' ); ?>
        </div>
    </div>
    <div class="ccp-content-container">
        <div class="ccp-info-container">
            <p>El proceso de Carga de una entrada o noticia a Wordpress es muy sencillo, aquí conseguirás una ayuda bastante simple de como hacerlo correctamente.</p>
            <p><strong>1.-</strong> Abrimos el WordPress, debe iniciar en la ventana de Escritorio, donde estan todas nuestras estadísiticas básicas, a su lado veremos el menú principal,
                donde encontraremos todas las funciones, tipos de entradas, paginas y opciones que nuestro sitio nos ofrece.</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/subir-post/paso01.jpg" alt="Paso 1 - Subir Entrada" />
            <p><strong>2.-</strong> Ingresamos desde el menu a la sección de Entradas, si colocamos el cursor encima de la opcion de "Entradas" del menú, se abrirá una serie de opciones adicionales,
                donde podemos agilizar la creación de las cosas que necesitamos; En este ejemplo usaremos la opcion de "Todas las Entradas".</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/subir-post/paso02.jpg" alt="Paso 2 - Subir Entrada" />
            <p><strong>3.-</strong> En la siguiente pantalla observaremos todas las entradas / noticias que hayamos ya subido a nuestro WordPress, en este caso seleccionaremos la opcion de "Añadir Nueva" para empezar a cargar nuestra nota.</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/subir-post/paso03.jpg" alt="Paso 3 - Subir Entrada" />
            <p><strong>4.-</strong> Ahora observaremos la pantalla principal para la carga de entradas a WordPress, esta consiste de varias partes: </p>
            <ol>
                <li>Sección principal para la carga del titulo e información del sitio.</li>
                <li>Sección para agregarle formato / categorias / etiquetas e imágen destacada a nuestra entrada.</li>
                <li>Sección para Publicar / Programar publicación de nuestra entrada.</li>
            </ol>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/subir-post/paso04.jpg" alt="Paso 4 - Subir Entrada" />
            <p><strong>5.-</strong> Empezamos con el título, en la parte superior del sitio, encontraremos el primer recuadro, donde debemos introducir el título que queremos colocar para nuestra noticia / entrada en WordPress. Nota: Es aconsejable no usar caracteres especiales en esta área que no sean absolutamente necesarios.</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/subir-post/paso05.jpg" alt="Paso 5 - Subir Entrada" />
            <p><strong>6.-</strong> Justo debajo conseguiremos el área para introducir el contenido de nuestra nota, aqui colocaremos el contenido que hayamos preparado ó podemos escribir directamente en ella y luego editar su estilo.</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/subir-post/paso06.jpg" alt="Paso 6 - Subir Entrada" />
            <p><strong>7.-</strong> Seguramente habremos pegado nuestro texto, pero ahora falta "vestirlo" con algunas cosas para que se vea de mejor manera, para esto usaremos la barra superior de herramientas para agregarle estilos a nuestro texto. La misma esta siempre oculta, aquí encontraras una breve descripción de las funciones de cada botón.</p>
            <p>NOTA: Debemos asegurarnos de estar en la vista "Visual" y no la "HTML"</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/subir-post/paso07.jpg" alt="Paso 7 - Subir Entrada" />
            <h3>Lista de Comandos:</h3>
            <ul>
                <li>Negrilla</li>
                <li>Itálica</li>
                <li>Tachado</li>
                <li>Lista Ordenada de Numeros</li>
                <li>Lista no ordenada de Puntos</li>
                <li>Bloque de Cita: Un parrafo con sangria y una linea caracteristica del lado izquierdo</li>
                <li>Línea Horizontal</li>
                <li>Botones de Alineación</li>
                <li>Agregar Hipervínculo</li>
                <li>Remover Hipervínculo</li>
                <li>Bloque para "Ver Más": Este elemento va a crear el "Extracto" de la nota, que es la versión corta de la entrada.</li>
                <li>Boton para ver mas opciones de la barra de herramientas.</li>
            </ul>
            <h4>Barra de Herramientas extra:</h4>
            <ul>
                <li>
                    Encabezados: Este se encarga de darle formato y/o tamaño a la letra del texto. Se divide en:
                    <ul>
                        <li>Parrafo => Por Defecto</li>
                        <li>H1 => Encabezado 1</li>
                        <li>H2 => Encabezado 2</li>
                        <li>H3 => Encabezado 3</li>
                        <li>H4 => Encabezado 4</li>
                        <li>H5 => Encabezado 5</li>
                        <li>H6 => Encabezado 6</li>
                        <li>Preformato => Texto plano con un preformato</li>

                    </ul>
                </li>
                <li>Subrayado</li>
                <li>Justificar</li>
                <li>Color de Texto</li>
                <li>Pegar como texto: Este elemento nos permite pegar texto copiado de otra fuente y pegarlo sin elementos especiales que puedan modificarlo</li>
                <li>Borrar Formato.</li>
                <li>Caracter Especial</li>
                <li>Sangría Derecha</li>
                <li>Sangría Izquierda</li>
                <li>Deshacer</li>
                <li>Rehacer</li>
                <li>Atajos de Teclado: una serie de comandos cortos de teclado que nos ayudaran a colocar los elementos rápidamente.</li>
            </ul>
            <p><strong>8.-</strong> Si deseamos agregar alguna fotografia dentro de la entrada que de alguna manera apoye nuestro texto en sí, usamos la opción de "Añadir Objeto", bajo esta opción podemos agregar tanto imagenes como archivos descargables como videos, documentos y archivos de audio.</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/subir-post/paso08.jpg" alt="Paso 8 - Subir Entrada" />
            <p><strong>9.-</strong> Aparecerá una ventana nueva, donde solamente tendriamos que arrastrar y colocar el archivo desde nuestra pc a esta pantalla para subir un archivo. De igualmanera esta disponible el boton de "Seleccionar Archivos" para tomar el archivo que necesitemos de nuestra pc.</p>
            <p>Tambien, si ya habiamos subido el archivo a nuestra pagina y queremos usar ese archivo previamente cargado, usamos la opcion de biblioteca multimedia.</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/subir-post/paso09.jpg" alt="Paso 9 - Subir Entrada" />
            <p><strong>10.-</strong> Ya una vez cargado nuestro archivo a la pagina o haber hecho clic en biblioteca multimedia para ver los archivos ya cargados al sitio, veremos una ventana como la siguiente, donde solo debemos hacer click en la fotografia o archivo que necesitemos y hagamos click en "Insertar en la Entrada"</p>
            <p>Antes de Eso podemos editar la metadata del archivo para agregarle mas información del mismo o editar su leyenda.</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/subir-post/paso10.jpg" alt="Paso 10 - Subir Entrada" />
            <p><strong>11.-</strong> Y asi ya tenemos cargada nuestra foto dentro de la nota de WordPress, puede repetir este procedimiento las veces que desee.</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/subir-post/paso11.jpg" alt="Paso 11 - Subir Entrada" />
            <p><strong>11a. -</strong> De esta misma manera agregaremos nuestra imagen destacada en el sitio, ubicada en la Sección 2 del sitio, hacemos click en "Agregar imagen Destacada" y seguimos los pasos anteriores.</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/subir-post/paso11a.jpg" alt="Paso 11a - Subir Entrada" />
            <p><strong>12.-</strong> Ahora el siguiente paso es asignarle categorias, formato y etiquetas y publicar nuestra nota para que sea publica al mundo. Para ello ahora trabajaremos con la barra derecha, donde nos aparecera la siguiente información.</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/subir-post/paso12.jpg" alt="Paso 12 - Subir Entrada" />

            <p><strong>13.-</strong> Para agregar Categorias, hacemos click en "Añadir Nueva Categoria" para que nos salga un cuadro de texto para poder ingresar el texto de nuestra categoria. Para salvar los cambios hacemos clic en el boton siguiente de "Añadir nueva Categoría" y ya estará creada nuestra categoría.</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/subir-post/paso13.jpg" alt="Paso 13 - Subir Entrada" />

            <p><strong>14.-</strong> Para agregar etiquetas, ingresamos nuestra etiqueta a destacar en nuestra entrada y la guardamos haciendo click en "Añadir", de esta manera ya la tendremos integrada, podemos agregar muchas etiquetas si asi requiera el sitio.</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/subir-post/paso14.jpg" alt="Paso 14 - Subir Entrada" />

            <p><strong>15.-</strong> Para finalizar, la sección 3 estará a cargo del estatus de nuestra entrada, aqui podemos ver si la entrada fue publicda o solo se guardó. </p>
            <p>Adicional a esto podemos programar la publicación de nuestra entrada, por si queremos agregarla en una fecha / hora específica. </p>
            <p>Para finalizar lo ideal es hacer click en publicar y asi terminamos de subir nuestra entrada.</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/subir-post/paso15.jpg" alt="Paso 15 - Subir Entrada" />

        </div>
        <div class="ccp-extra-info-container">
            <div class="ccp-sticky-menu">
                <h3 class="sectiontitle extra-menu-title">Manuales Adicionales</h3>
                <a href="<?php echo esc_url(admin_url('/admin.php?page=subir-entrada')); ?>">
                    <div class="ccp-function-item ccp-function-item-5">
                        <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon5.png" alt="" />
                        <h3>Subir Entrada / Noticia</h3>
                    </div>
                </a>
                <a href="<?php echo esc_url(admin_url('/admin.php?page=editar-pagina')); ?>">
                    <div class="ccp-function-item ccp-function-item-6">
                        <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon11.png" alt="">
                        <h3>Editar Página</h3>
                    </div>
                </a>
                <a href="<?php echo esc_url(admin_url('/admin.php?page=editar-menu')); ?>">
                    <div class="ccp-function-item ccp-function-item-1">
                        <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon12.png" alt="">
                        <h3>Editar Menu del sitio</h3>
                    </div>
                </a>
                <a href="<?php echo esc_url(admin_url('/admin.php?page=crear-usuarios')); ?>">
                    <div class="ccp-function-item ccp-function-item-2">
                        <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon13.png" alt="">
                        <h3>Crear usuarios de WordPress</h3>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
