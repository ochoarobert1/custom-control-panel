<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.robertochoa.com.ve/
 * @since      1.0.0
 *
 * @package    Custom_Control_Panel
 * @subpackage Custom_Control_Panel/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php $themes = wp_get_theme (); ?>

<div class="ccp-main-container">
    <div class="ccp-header-container">
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-white.png" alt="Logo" class="ccp-img-brand">
        <div class="ccp-header-info">
            <h1 class="sectiontitle">Crear Usuarios en WordPress</h1>
            <p>Manual para la creación de editores o administradores en el sitio.</p>
        </div>
        <div class="ccp-header-extra">
            <a href="<?php echo esc_url(admin_url('/themes.php?theme=' . get_template())); ?>" title="Ver información del Theme"><p><?php echo $themes->name; ?></p></a>
            <?php echo do_action( 'admin_survey' ); ?>
        </div>
    </div>
    <div class="ccp-content-container">
        <div class="ccp-info-container">
            <p>Para tener un mejor control de cambios y de accesos, WordPress nos permite crear diversos usuarios dentro del sitio, cada uno puede tener permisos de editar y pueden tambien tener restricciones de ver àreas administrativas del sitio.</p>
            <p>Para crear un nuevo usuario debemos hacer lo siguiente:</p>
            <p><strong>1.-</strong> Abrimos el WordPress, debe iniciar en la ventana de Escritorio, donde estan todas nuestras estadísiticas básicas, a su lado veremos el menú principal, donde encontraremos todas las funciones, tipos de entradas, paginas y opciones que nuestro sitio nos ofrece, aquí ingresamos en la sección "Usuarios" y hacemos click en "Todos los Usuarios".</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/crear-usuarios/paso01.jpg" alt="Paso 1 - Editar Página" />
            <p><strong>2.-</strong> Aquí tenemos los usuarios que ya estan creados en el sistema, es importante nunca eliminar Administradores a menos que la persona haya dejado de trabajar en la organizaciòn y es aun màs importante no tocar el usuario de "super administrador" que se genera al momento de crear la pàgina.</p>
            <p>Sabiendo esto, hacemos click en "Añadir Nuevo"</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/crear-usuarios/paso02.jpg" alt="Paso 2 - Editar Página" />
            <p><strong>3.-</strong> En esta ventana observaremos la ventana donde ingresaremos los datos de nuestro usuario, rellenamos los datos con detalle, es importante poder rellenar todos los campos, de ser posible.</p>
            <p><strong>NOTA:</strong> Es importante usar un uso estandar de los nombre de usuarios, en este caso usamos como ejemplo el metodo siguiente</p>
            <ul>
                <li>x.apellido</li>
                <li>x_apellido</li>
            </ul>
            <h6>* Donde x es la inicial del nombre</h6>
            <p>No se recomienda colocar nombres con espacios ni nombres completos.</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/crear-usuarios/paso03.jpg" alt="Paso 3 - Editar Página" />
            <p><strong>4.-</strong> Una vez hallamos rellenado toda la información del usuario, podemos disponernos a elegirle una contraseña de ingreso. Para ello, podemos dejar que WordPress le asigne una clave super fuerte ó podemos elegir la contraseña que el usuario va a usar, solo debemos hacer click en "Mostrar contraseña" y podemos ingresar nuestra combinación. <strong>Nota: </strong>siempre es ideal colocar una contraseña media / fuerte que nos ofrezca seguridad.</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/crear-usuarios/paso04.jpg" alt="Paso 4 - Editar Página" />
            <p><strong>5.-</strong> Solo nos queda ingresar el Perfil del usuario, este perfil determina cuanto acceso va a tener el mismo a las diferentes áreas del WordPress, los perfiles se dividen en:</p>
            <ul>
                <li>Administrador: Puede manejar todas las funciones del WordPress sin restricciones.</li>
                <li>Editor: Puede editar las paginas / entradas / entradas personalizadas, ademas de poder cambiar Menús y Widgets del sitio. (Este es el usuario más estandar del sitio).</li>
                <li>Autor: Puede solo crear entradas en el WordPress</li>
                <li>Colaborador: Solo puede editar entradas, no puede crearlas</li>
                <li>Suscriptor: Solo puede ver las entradas, no puede editar ni crear.</li>
            </ul>
            <p>Seleccionamos de nuestro selector la opción acorde a lo que necesita el usuario y hacemos click en "Añadir nuevo Usuario"</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/crear-usuarios/paso05.jpg" alt="Paso 5 - Editar Página" />
            <p>Los cambios se reflejarán automaticamente y habremos creado correctamente nuestro usuario.</p>
        </div>
        <div class="ccp-extra-info-container">
            <div class="ccp-sticky-menu">
                <h3 class="sectiontitle extra-menu-title">Manuales Adicionales</h3>
                <a href="<?php echo esc_url(admin_url('/admin.php?page=subir-entrada')); ?>">
                    <div class="ccp-function-item ccp-function-item-5">
                        <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon5.png" alt="" />
                        <h3>Subir Entrada / Noticia</h3>
                    </div>
                </a>
                <a href="<?php echo esc_url(admin_url('/admin.php?page=editar-pagina')); ?>">
                    <div class="ccp-function-item ccp-function-item-6">
                        <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon11.png" alt="">
                        <h3>Editar Página</h3>
                    </div>
                </a>
                <a href="<?php echo esc_url(admin_url('/admin.php?page=editar-menu')); ?>">
                    <div class="ccp-function-item ccp-function-item-1">
                        <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon12.png" alt="">
                        <h3>Editar Menu del sitio</h3>
                    </div>
                </a>
                <a href="<?php echo esc_url(admin_url('/admin.php?page=crear-usuarios')); ?>">
                    <div class="ccp-function-item ccp-function-item-2">
                        <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon13.png" alt="">
                        <h3>Crear usuarios de WordPress</h3>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
