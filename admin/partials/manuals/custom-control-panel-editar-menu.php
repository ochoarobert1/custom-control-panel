<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.robertochoa.com.ve/
 * @since      1.0.0
 *
 * @package    Custom_Control_Panel
 * @subpackage Custom_Control_Panel/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php $themes = wp_get_theme (); ?>

<div class="ccp-main-container">
    <div class="ccp-header-container">
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-white.png" alt="Logo" class="ccp-img-brand">
        <div class="ccp-header-info">
            <h1 class="sectiontitle">Editar Menu del Sitio</h1>
            <p>Manual para edición del menu principal / menu de pie de página del sitio.</p>
        </div>
        <div class="ccp-header-extra">
            <a href="<?php echo esc_url(admin_url('/themes.php?theme=' . get_template())); ?>" title="Ver información del Theme"><p><?php echo $themes->name; ?></p></a>
            <?php echo do_action( 'admin_survey' ); ?>
        </div>
    </div>
    <div class="ccp-content-container">
        <div class="ccp-info-container">
            <p>El menú principal y el menú del pie de página tienen un mismo sistema de edición, el cual vamos a ver a continuación, es importante saber que esto va a depender de la anchura de nuestro sitio, no es recomendable colocar muchisimas opciones que alargen el sitio de manera poco estética.</p>
            <p>Ahora bien, para editar o crear un nuevo menú debemos seguir los pasos siguientes:</p>
            <p><strong>1.-</strong> Abrimos el WordPress, debe iniciar en la ventana de Escritorio, donde estan todas nuestras estadísiticas básicas, a su lado veremos el menú principal, donde encontraremos todas las funciones, tipos de entradas, paginas y opciones que nuestro sitio nos ofrece, aquí ingresamos en la sección "Apariencia" y hacemos click en "Menús".</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/editar-menu/paso01.jpg" alt="Paso 1 - Editar Página" />
            <p><strong>2.-</strong> Aquí tenemos un ejemplo de un sitio con un solo menú, podremos elegir crear un nuevo menú o editar algun otro, es importante saber que menu queremos editar para no cometer errores. Observamos que del lado izquierdo tenemos todas las opciones que podemos agregar, las cuales son:</p>
            <ul>
                <li>Entradas (Noticias / Posts)</li>
                <li>Páginas</li>
                <li>Tipos de entradas personalizadas (Post - Noticias - Miembros - Eventos - etc.)</li>
                <li>Enlace Personalizado (en caso que querramos colocar un enlace externo o interno en especifico como opción en el menú)</li>
                <li>Categorías</li>
            </ul>
            <p>Del lado izquierdo podemos ver que tenemos las opciones de nuestro menú actual en orden, este orden puede ser alterado haciendo "drag &amp; drop" o en español "Arrastrar y colocar" y la sección del theme donde se puede ubicar este menu, por lo general siempre son dos zonas: Header (Cabecera) y Footer (Pie de Página)</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/editar-menu/paso02.jpg" alt="Paso 2 - Editar Página" />
            <p><strong>3.-</strong> En el menu izquierdo elegimos la opción que querramos colocar, bien sea pagina, post, categoria, etc. Al seleccionar, lo ideal es darle click a "Ver Todo" para así tener el listado completo de los elementos de esta opción.</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/editar-menu/paso03.jpg" alt="Paso 3 - Editar Página" />
            <p><strong>4.-</strong> Debemos hacer click en el cuadro de verificación de los elementos que querramos ingresar a nuestro menu y hacemos click en "Añadir a Menu" <strong>Nota:</strong> Podemos agregar varias opciones a la vez</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/editar-menu/paso04.jpg" alt="Paso 4 - Editar Página" />
            <p><strong>5.-</strong> Observaremos que nuestra opción aparece en el menu derecho, solo nos queda ubicarla en el orden que querramos que aparezca.</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/editar-menu/paso05.jpg" alt="Paso 5 - Editar Página" />
            <p><strong>6.-</strong> Verificamos que quede en el orden y en el lugar que queremos que se muestre, esto se hace eligiendo en el campo inferior la Ubicación en el tema.</p>
            <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/editar-menu/paso06.jpg" alt="Paso 6 - Editar Página" />
            <p><strong>7.-</strong> Al estar seguros de que todo esté bien y ya logremos elegir y añadir los elementos que necesitamos, hacemos click en "Guardar Menu".</p>
             <img src="<?php echo esc_url(plugins_url()) . '/' . $this->plugin_name; ?>/assets/manuales/editar-menu/paso07.jpg" alt="Paso 7 - Editar Página" />
            <p>los cambios se reflejarán automaticamente y habremos editado correctamente nuestro menú.</p>

        </div>
        <div class="ccp-extra-info-container">
            <div class="ccp-sticky-menu">
                <h3 class="sectiontitle extra-menu-title">Manuales Adicionales</h3>
                <a href="<?php echo esc_url(admin_url('/admin.php?page=subir-entrada')); ?>">
                    <div class="ccp-function-item ccp-function-item-5">
                        <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon5.png" alt="" />
                        <h3>Subir Entrada / Noticia</h3>
                    </div>
                </a>
                <a href="<?php echo esc_url(admin_url('/admin.php?page=editar-pagina')); ?>">
                    <div class="ccp-function-item ccp-function-item-6">
                        <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon11.png" alt="">
                        <h3>Editar Página</h3>
                    </div>
                </a>
                <a href="<?php echo esc_url(admin_url('/admin.php?page=editar-menu')); ?>">
                    <div class="ccp-function-item ccp-function-item-1">
                        <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon12.png" alt="">
                        <h3>Editar Menu del sitio</h3>
                    </div>
                </a>
                <a href="<?php echo esc_url(admin_url('/admin.php?page=crear-usuarios')); ?>">
                    <div class="ccp-function-item ccp-function-item-2">
                        <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon13.png" alt="">
                        <h3>Crear usuarios de WordPress</h3>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>
