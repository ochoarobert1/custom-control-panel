<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.robertochoa.com.ve/
 * @since      1.0.0
 *
 * @package    Custom_Control_Panel
 * @subpackage Custom_Control_Panel/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php $themes = wp_get_theme (); ?>

<div class="ccp-main-container">
    <div class="ccp-header-container">
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-white.png" alt="Logo" class="ccp-img-brand">
        <div class="ccp-header-info">
            <h1 class="sectiontitle">Geoposición en Google Maps</h1>
            <p>Ubicación de coordenadas de la empresa.</p>
        </div>
        <div class="ccp-header-extra">
            <a href="<?php echo esc_url(admin_url('/themes.php?theme=' . get_template())); ?>" title="Ver información del Theme"><p><?php echo $themes->name; ?></p></a>
            <?php echo do_action( 'admin_survey' ); ?>
        </div>
    </div>
    <div class="ccp-content-container">
        <div class="ccp-section-title">
            <h2>Opciones Generales</h2>
        </div>
        <div class="ccp-data-container">
            <div class="ccp-info-container">
                <div id="map"></div>
                <span id="ccp-loader"></span>
                <form id="maphidden">
                    <input type="hidden" id="latitude" name="latitude" value="" />
                    <input type="hidden" id="longitude" name="longitude" value="" />
                </form>
                <button onclick="ccp_save_maps()" class="btn-save">Guardar Cambios</button>
            </div>
            <div class="ccp-extra-info-container">
                <div class="ccp-sticky-menu">
                    <h3 class="sectiontitle extra-menu-title">Manuales Adicionales</h3>
                    <a href="<?php echo esc_url(admin_url('/admin.php?page=subir-entrada')); ?>">
                        <div class="ccp-function-item ccp-function-item-5">
                            <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon5.png" alt="" />
                            <h3>Subir Entrada / Noticia</h3>
                        </div>
                    </a>
                    <a href="<?php echo esc_url(admin_url('/admin.php?page=editar-pagina')); ?>">
                        <div class="ccp-function-item ccp-function-item-6">
                            <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon11.png" alt="">
                            <h3>Editar Página</h3>
                        </div>
                    </a>
                    <a href="<?php echo esc_url(admin_url('/admin.php?page=editar-menu')); ?>">
                        <div class="ccp-function-item ccp-function-item-1">
                            <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon12.png" alt="">
                            <h3>Editar Menu del sitio</h3>
                        </div>
                    </a>
                    <a href="<?php echo esc_url(admin_url('/admin.php?page=crear-usuarios')); ?>">
                        <div class="ccp-function-item ccp-function-item-2">
                            <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon13.png" alt="">
                            <h3>Crear usuarios de WordPress</h3>
                        </div>
                    </a>
                </div>
            </div>
        </div>

        <script>
            function initMap() {
                <?php $multivalue = get_option( '_ccp_map_latitude' ); ?>;
                <?php if ($multivalue == '') { $multivalue = '4.6486259'; } ?>
                var savedLat = <?php echo $multivalue; ?>;
                <?php $multivalue = get_option( '_ccp_map_longitude' ); ?>;
                <?php if ($multivalue == '') { $multivalue = '-74.2478935'; } ?>
                var savedLnt = <?php echo $multivalue; ?>;
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 5,
                    center: {lat: savedLat, lng: savedLnt}
                });
                var marker = new google.maps.Marker({
                    position:  {lat: savedLat, lng: savedLnt},
                    map: map
                });
                // double click event
                google.maps.event.addListener(map, 'dblclick', function(e) {
                    var positionDoubleclick = e.latLng;
                    marker.setPosition(positionDoubleclick);
                    // if you don't do this, the map will zoom in
                    var lat = marker.getPosition().lat();
                    var lng = marker.getPosition().lng();
                    jQuery('#latitude').val(lat);
                    jQuery('#longitude').val(lng);
                });
            }
        </script>
        <script async defer type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA5LzXsdYBl1bfqoKD7u-OHvj5rfjAwovs&signed_in=true&libraries=places&callback=initMap"></script>
    </div>
</div>
