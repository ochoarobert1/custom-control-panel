<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.robertochoa.com.ve/
 * @since      1.0.0
 *
 * @package    Custom_Control_Panel
 * @subpackage Custom_Control_Panel/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php $themes = wp_get_theme (); ?>

<div class="ccp-main-container">
    <div class="ccp-header-container">
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-white.png" alt="Logo" class="ccp-img-brand">
        <div class="ccp-header-info">
            <h1 class="sectiontitle">Datos de Contacto</h1>
            <p>Área custom para guardar los datos empresariales del cliente y poder usarlos en el sitio.</p>
        </div>
        <div class="ccp-header-extra">
            <a href="<?php echo esc_url(admin_url('/themes.php?theme=' . get_template())); ?>" title="Ver información del Theme"><p><?php echo $themes->name; ?></p></a>
            <?php echo do_action( 'admin_survey' ); ?>
        </div>
    </div>
    <div class="ccp-content-container">
        <div class="ccp-data-container">
            <div class="ccp-info-container">
                <div class="ccp-section-title">
                    <h2>Datos de Contacto / Redes Sociales</h2>
                </div>
                <div class="ccp-data-field">
                    <h5>Datos Generales</h5>
                    <div class="ccp-data-item">
                        <span>RIF Cliente:</span>
                        <?php $client_rif = get_option( '_ccp_client_rif' ); ?>
                        <?php if ($client_rif == false) { $artists_text = ""; } ?>
                        <input id="ccp_client_rif" name="ccp_client_rif"  type="text" placeholder="Muestra: J-30644745-8" value="<?php echo $client_rif; ?>" />
                    </div>
                    <div class="ccp-data-item">
                        <span>Nombre del Cliente:</span>
                        <?php $client_name = get_option( '_ccp_client_name' ); ?>
                        <?php if ($client_name == false) { $client_name = ""; } ?>
                        <input id="ccp_client_name" name="ccp_client_name"  type="text" placeholder="Nombre de la Organización" value="<?php echo $client_name; ?>" />
                    </div>
                    <div class="ccp-data-item">
                        <span>Correo Electrónico del Cliente:</span>
                        <?php $client_mail = get_option( '_ccp_client_mail' ); ?>
                        <?php if ($client_mail == false) { $client_mail = ""; } ?>
                        <input id="ccp_client_mail" name="ccp_client_mail"  type="text" placeholder="Muestra: correo@dominio.com" value="<?php echo $client_mail; ?>" />
                    </div>
                    <div class="ccp-data-item">
                        <span>Teléfonos del Cliente:</span>
                        <?php $client_telf = get_option( '_ccp_client_telf' ); ?>
                        <?php if ($client_telf == false) { $client_telf = ""; } ?>
                        <input id="ccp_client_telf" name="ccp_client_telf"  type="text" placeholder="Muestra: +58-212-xxx-xxxx / +58-212-xxx-xxxx" value="<?php echo $client_telf; ?>" />
                    </div>
                    <div class="ccp-data-item">
                        <p>Dirección del Cliente:</p>
                        <?php $client_memo = get_option( '_ccp_client_dir' ); ?>
                        <?php if ($client_memo == false) { $client_memo = ""; } ?>
                        <textarea id="ccp_client_dir" name="ccp_client_dir" type="text" placeholder="Dirección de la Organización"><?php echo $client_memo; ?></textarea>
                    </div>
                </div>
                <span id="ccp-loader"></span>
                <button onclick="ccp_save_options()" class="btn-save">Guardar Cambios</button>
                <div class="ccp-data-field">
                    <h5>Redes Sociales</h5>
                    <div class="ccp-data-item">
                        <span>Facebook:</span>
                        <?php $client_fb = get_option( '_ccp_facebook_profile' ); ?>
                        <?php if ($client_fb == false) { $client_fb = ""; } ?>
                        <input id="ccp_facebook_profile" name="ccp_facebook_profile"  type="text" placeholder="Muestra: https://www.facebook.com/username" value="<?php echo $client_fb; ?>" />
                    </div>
                    <div class="ccp-data-item">
                        <span>Twitter:</span>
                        <?php $client_tw = get_option( '_ccp_twitter_profile' ); ?>
                        <?php if ($client_tw == false) { $client_tw = ""; } ?>
                        <input id="ccp_twitter_profile" name="ccp_twitter_profile"  type="text" placeholder="Muestra: https://www.twitter.com/username" value="<?php echo $client_tw; ?>" />
                    </div>
                    <div class="ccp-data-item">
                        <span>Instagram:</span>
                        <?php $client_ig = get_option( '_ccp_instagram_profile' ); ?>
                        <?php if ($client_ig == false) { $client_ig = ""; } ?>
                        <input id="ccp_instagram_profile" name="ccp_instagram_profile"  type="text" placeholder="Muestra: https://www.instagram.com/username" value="<?php echo $client_ig; ?>" />
                    </div>
                    <div class="ccp-data-item">
                        <span>Google Plus:</span>
                        <?php $client_gp = get_option( '_ccp_google_profile' ); ?>
                        <?php if ($client_gp == false) { $client_gp = ""; } ?>
                        <input id="ccp_google_profile" name="ccp_google_profile"  type="text" placeholder="Muestra: https://plus.google.com/+username" value="<?php echo $client_gp; ?>" />
                    </div>
                    <div class="ccp-data-item">

                        <span>Youtube:</span>
                        <?php $client_yt = get_option( '_ccp_youtube_profile' ); ?>
                        <?php if ($client_yt == false) { $client_yt = ""; } ?>
                        <input id="ccp_youtube_profile" name="ccp_youtube_profile"  type="text" placeholder="Muestra:  https://www.youtube.com/channel/youtube_channel" value="<?php echo $client_yt; ?>" />
                    </div>
                    <div class="ccp-data-item">
                        <span>Soundcloud:</span>
                        <?php $client_sc = get_option( '_ccp_soundcloud_profile' ); ?>
                        <?php if ($client_sc == false) { $client_sc = ""; } ?>
                        <input id="ccp_soundcloud_profile" name="ccp_soundcloud_profile"  type="text" placeholder="Muestra:  https://soundcloud.com/username" value="<?php echo $client_sc; ?>" />
                    </div>

                </div>
                <span id="ccp-loader2"></span>
                <button onclick="ccp_save_options()" class="btn-save">Guardar Cambios</button>
            </div>
            <div class="ccp-extra-info-container">
                <div class="ccp-sticky-menu">
                    <h3 class="sectiontitle extra-menu-title">Manuales Adicionales</h3>
                    <a href="<?php echo esc_url(admin_url('/admin.php?page=subir-entrada')); ?>">
                        <div class="ccp-function-item ccp-function-item-5">
                            <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon5.png" alt="" />
                            <h3>Subir Entrada / Noticia</h3>
                        </div>
                    </a>
                    <a href="<?php echo esc_url(admin_url('/admin.php?page=editar-pagina')); ?>">
                        <div class="ccp-function-item ccp-function-item-6">
                            <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon11.png" alt="">
                            <h3>Editar Página</h3>
                        </div>
                    </a>
                    <a href="<?php echo esc_url(admin_url('/admin.php?page=editar-menu')); ?>">
                        <div class="ccp-function-item ccp-function-item-1">
                            <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon12.png" alt="">
                            <h3>Editar Menu del sitio</h3>
                        </div>
                    </a>
                    <a href="<?php echo esc_url(admin_url('/admin.php?page=crear-usuarios')); ?>">
                        <div class="ccp-function-item ccp-function-item-2">
                            <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon13.png" alt="">
                            <h3>Crear usuarios de WordPress</h3>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
