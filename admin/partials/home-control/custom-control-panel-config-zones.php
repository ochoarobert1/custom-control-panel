<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.robertochoa.com.ve/
 * @since      1.0.0
 *
 * @package    Custom_Control_Panel
 * @subpackage Custom_Control_Panel/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<?php global $wpdb; ?>
<?php $themes = wp_get_theme(); ?>
<div class="ccp-main-container">
    <div class="ccp-header-container">
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-white.png" alt="Logo" class="ccp-img-brand">
        <div class="ccp-header-info">
            <h1 class="sectiontitle">Ajustar Inicio</h1>
            <p>Áquí controlamos el orden de los bloques de noticias y publicidad.</p>
        </div>
        <div class="ccp-header-extra">
            <a href="<?php echo esc_url(admin_url('/themes.php?theme=' . get_template())); ?>" title="Ver información del Theme"><p><?php echo $themes->name; ?></p></a>
            <?php echo do_action( 'admin_survey' ); ?>
        </div>
    </div>
    <div class="ccp-content-container">
        <div class="ccp-section-title">
            <h2>Seleccione el bloque que quiere ajustar de posición</h2>
        </div>
        <div class="ccp-clearfix"></div>
        <div class="ccp-info-container">
            <ul class="ui-sortable" id="sortable">
                <?php $fivesdrafts = $wpdb->get_results( "SELECT * FROM " . $wpdb->prefix . "cc_blocks ORDER BY posicion ASC"); ?>
                <?php $orderblocks = array(); $i=1; ?>
                <?php foreach ( $fivesdrafts as $fivesdraft ) { ?>
                <?php $orderblocks[$i] = $fivesdraft->Id; ?>
                <li id="destacado-<?php echo $fivesdraft->Id; ?>" class="ui-state-default"><i class="ccp-item-close">x</i> <?php echo $fivesdraft->titulo; ?> <span>(Categoría: <?php echo $fivesdraft->categoria; ?>)</span></li>
                <?php $i++; } ?>
            </ul>
            <input id="orderBlocks" type="hidden" name="orderBlocks" value='<?php echo implode(",", $orderblocks); ?>' />
            <button onclick="add_news()" class="btn-zone-add">Agregar Bloque de Noticias</button>
            <button onclick="add_ads()"  class="btn-zone-add">Agregar Bloque de Publicidad</button>
            <div class="ccp-clearfix"></div>
            <div class="ccp-ajax-helper"></div>
            <button onclick="save_blocks()" class="btn-save-changes">Guardar Cambios</button>
        </div>
        <div class="ccp-extra-info-container">
            <div class="ccp-sticky-menu">
                <h3 class="sectiontitle extra-menu-title">Instrucciones de Uso</h3>

                <ol>
                    <li>Seleccione la zona que quiere ajustar de posición. Este controlador funciona via Drag n Drop, seleccione su elemente y arrastrelo hasta la posición deseada</li>
                    <li>Si necesita agregar un bloque de noticias o de publicidad, haga click en sus respectivos botones, estos generarán un nuevo bloque dependiendo de su elección</li>
                    <li>Al terminar de ajustar el inicio, haga click en "Guardar Cambios" para guardar sus ajustes.</li>
                </ol>

            </div>
        </div>
        <div class="ccp-clearfix"></div>
    </div>
</div>
