<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.robertochoa.com.ve/
 * @since      1.0.0
 *
 * @package    Custom_Control_Panel
 * @subpackage Custom_Control_Panel/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<?php global $wpdb; ?>
<?php $themes = wp_get_theme(); ?>
<div class="ccp-main-container">
    <div class="ccp-header-container">
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-white.png" alt="Logo" class="ccp-img-brand">
        <div class="ccp-header-info">
            <h1 class="sectiontitle">Ajustar Inicio</h1>
            <p>Áquí controlamos el orden de los bloques de noticias y publicidad.</p>
        </div>
        <div class="ccp-header-extra">
            <a href="<?php echo esc_url(admin_url('/themes.php?theme=' . get_template())); ?>" title="Ver información del Theme"><p><?php echo $themes->name; ?></p></a>
            <?php echo do_action( 'admin_survey' ); ?>
        </div>
    </div>
    <div class="ccp-content-container">
        <div class="ccp-section-title">
            <h2>Seleccione el bloque que quiere ajustar de posición</h2>
        </div>
        <div class="ccp-clearfix"></div>
        <div class="ccp-info-container">
            <div class="ccp-tabs-blocks" id="tabs">
                <ul>
                    <?php $bloques = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "cc_blocks"); ?>
                    <?php foreach ( $bloques as $bloque ) { ?>
                    <li><a href="#tabs-<?php echo $bloque->Id; ?>"><?php echo $bloque->titulo; ?></a></li>
                    <?php } ?>
                </ul>
                <div id="tabs-1">
                   <div><input type="text"></div>
                </div>
            </div>
        </div>
        <div class="ccp-extra-info-container">
            <div class="ccp-sticky-menu">
                <h3 class="sectiontitle extra-menu-title">Instrucciones de Uso</h3>



            </div>
        </div>
        <div class="ccp-clearfix"></div>
    </div>
</div>
