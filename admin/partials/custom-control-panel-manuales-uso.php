<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://www.robertochoa.com.ve/
 * @since      1.0.0
 *
 * @package    Custom_Control_Panel
 * @subpackage Custom_Control_Panel/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php $themes = wp_get_theme (); ?>

<div class="ccp-main-container">
    <div class="ccp-header-container">
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-white.png" alt="Logo" class="ccp-img-brand">
        <div class="ccp-header-info">
            <h1 class="sectiontitle">Manuales de Uso</h1>
            <p>Seríe de instructivos para las funciones del sitio.</p>
        </div>
        <div class="ccp-header-extra">
            <a href="<?php echo esc_url(admin_url('/themes.php?theme=' . get_template())); ?>" title="Ver información del Theme"><p><?php echo $themes->name; ?></p></a>
            <?php echo do_action( 'admin_survey' ); ?>
        </div>
    </div>
    <div class="ccp-content-container">
        <div class="ccp-section-title">
            <h2>Manuales de Uso</h2>
        </div>
        <div class="ccp-function-container">

                <a href="<?php echo esc_url(admin_url('/admin.php?page=subir-entrada')); ?>">
                    <div class="ccp-function-item ccp-function-item-5">
                        <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon5.png" alt="" />
                        <h3>Subir Entrada / Noticia</h3>
                    </div>
                </a>
                <a href="<?php echo esc_url(admin_url('/admin.php?page=editar-pagina')); ?>">
                    <div class="ccp-function-item ccp-function-item-6">
                        <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon11.png" alt="">
                        <h3>Editar Página</h3>
                    </div>
                </a>
                <a href="<?php echo esc_url(admin_url('/admin.php?page=editar-menu')); ?>">
                    <div class="ccp-function-item ccp-function-item-1">
                        <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon12.png" alt="">
                        <h3>Editar Menu del sitio</h3>
                    </div>
                </a>
                <a href="<?php echo esc_url(admin_url('/admin.php?page=crear-usuarios')); ?>">
                    <div class="ccp-function-item ccp-function-item-2">
                        <img src="<?php echo plugins_url() . '/' . $this->plugin_name; ?>/admin/img/icon13.png" alt="">
                        <h3>Crear usuarios de WordPress</h3>
                    </div>
                </a>

        </div>
    </div>
</div>
