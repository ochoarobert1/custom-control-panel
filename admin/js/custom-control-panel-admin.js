(function ($) {
    'use strict';

    /**
     * All of the code for your admin-facing JavaScript source
     * should reside in this file.
     *
     * Note: It has been assumed you will write jQuery code here, so the
     * $ function reference has been prepared for usage within the scope
     * of this function.
     *
     * This enables you to define handlers, for when the DOM is ready:
     *
     * $(function() {
     *
     * });
     *
     * When the window is loaded:
     *
     * $( window ).load(function() {
     *
     * });
     *
     * ...and/or other possibilities.
     *
     * Ideally, it is not considered best practise to attach more than a
     * single DOM-ready or window-load handler for a particular page.
     * Although scripts in the WordPress core, Plugins and Themes may be
     * practising this, we should strive to set a better example in our own work.
     */

    $(document).ready(function () {
        $(".sectiontitle").lettering('words');
        $(".ccp-sticky-menu").sticky({topSpacing: 50});
        $( "#sortable" ).sortable({
            placeholder: "ui-sortable-placeholder"
        });
        $( "#tabs" ).tabs();
    });

})(jQuery);

function ccp_news_fetcher() {
    "use strict";
    var artistsText = jQuery('#artists_text').val(), servicesText = jQuery('#services_text').val();
    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + "/includes/class-custom-control-panel-save-custom.php",
        data: {
            artistsText: artistsText,
            servicesText: servicesText
        },
        beforeSend: function () {
            jQuery('.ccp-alert-box').html('<span class="ccp-loading"></span>');
        },
        success: function (resp) {
            jQuery('.ccp-alert-box').html(resp + '<p>Entradas Actualizadas !</p>');

        },
        error: function () {
            alert('error');
        }
    });
    return false;
}

function ccp_save_options() {
    "use strict";
    var artistsText = jQuery('#artists_text').val(), servicesText = jQuery('#services_text').val();
    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + "/includes/class-custom-control-panel-save-custom.php",
        data: {
            artistsText: artistsText,
            servicesText: servicesText
        },
        beforeSend: function () {
            jQuery('#ccp-loader').html('<span class="ccp-loading"></span>');
        },
        success: function (resp) {
            jQuery('#ccp-loader').html(resp + '<p>Entradas Actualizadas !</p>');

        },
        error: function () {
            alert('error');
        }
    });
    return false;

}

function ccp_save_codes() {
    "use strict";
    var ccp_google_code = jQuery('#ccp_google_code').val(), ccp_facebook_code = jQuery('#ccp_facebook_code').val();
    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + "/includes/class-custom-control-panel-save-custom.php",
        data: {
            ccp_google_code: ccp_google_code,
            ccp_facebook_code: ccp_facebook_code,
        },
        beforeSend: function () {
            jQuery('#ccp-loader').html('<span class="ccp-loading"></span>');
        },
        success: function (resp) {
            jQuery('#ccp-loader').html('<p>Códigos Actualizados !</p>');
        },
        error: function () {
            alert('error');
        }
    });
    return false;
}

function ccp_save_maps() {
    "use strict";
    var form = jQuery('#maphidden');
    jQuery.ajax({
        type: 'POST',
        url: ruta_blog + "/includes/class-custom-control-panel-save-custom.php",
        data: form.serialize(),
        beforeSend: function () {
            jQuery('#ccp-loader').html('<span class="ccp-loading"></span>');
        },
        success: function (resp) {
            jQuery('#ccp-loader').html('<p>Ubicación Actualizada</p>');
        },
        error: function () {
            alert('error');
        }
    });
    return false;
}

/* BLOCKS */
function add_news() {
    var data = jQuery('#sortable').sortable('serialize');
    var res = data.split("&");
    var counter = res.length + 1;
    console.log(counter);
    jQuery('#sortable').append('<li id="destacado-' + counter + '" class="ui-state-default"><i class="ccp-item-close">x</i> Nuevo Bloque <span></span></li>');
}

function add_ads() {
    jQuery('#sortable').append('<li class="ui-state-default">Publicidad</li>');
}

function save_blocks() {
    var data = jQuery('#sortable').sortable('serialize');
    var jsonPos = jQuery('#orderBlocks').val();
    //var jsonPos = 1;
    jQuery.ajax({
        type: 'POST',
        data: {
            datos: data,
            posiciones: jsonPos,
        },
        url: ruta_blog + '/includes/class-custom-control-panel-save-blocks.php',
        beforeSend: function () {
            jQuery('.ccp-ajax-helper').html('<span class="ccp-loading"></span>');
        },
        success: function (resp) {
            jQuery('.ccp-ajax-helper').html('<p>Ubicación Actualizada</p>');
        }
    });
}
