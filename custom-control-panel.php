<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.robertochoa.com.ve/
 * @since             1.0.0
 * @package           Custom_Control_Panel
 *
 * @wordpress-plugin
 * Plugin Name:       Panel de Control
 * Plugin URI:        custom-control-panel
 * Description:       Panel de control y Manuales de Uso del Theme Custom
 * Version:           1.0.0
 * Author:            Robert Ochoa
 * Author URI:        http://www.robertochoa.com.ve/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       custom-control-panel
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-custom-control-panel-activator.php
 */
function activate_custom_control_panel() {
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-custom-control-panel-activator.php';
    Custom_Control_Panel_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-custom-control-panel-deactivator.php
 */
function deactivate_custom_control_panel() {
    require_once plugin_dir_path( __FILE__ ) . 'includes/class-custom-control-panel-deactivator.php';
    Custom_Control_Panel_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_custom_control_panel' );
register_deactivation_hook( __FILE__, 'deactivate_custom_control_panel' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-custom-control-panel.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_custom_control_panel() {

    $plugin = new Custom_Control_Panel();
    $plugin->run();

}
run_custom_control_panel();
